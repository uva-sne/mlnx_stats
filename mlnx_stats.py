#!/usr/bin/python3

import subprocess, sys, time

def get_statistics(interface):
    cmd = ['ethtool', '-S', interface]
    out = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout,stderr = out.communicate()
    values = {}

    if (stderr == None):
        output = stdout.decode("utf-8")
        lines = output.split('\n')[1:-1]

        for line in lines:
            [key,value] = line.split(':', 1)
            values[key.strip()] = int(value)
    else:
        print(stderr)

    return values

def get_rates(prev, curr, delta):
    rates = {}
    for key in curr:
        rates[key] = (curr[key] - prev[key]) / delta
    return rates

def get_totals(orig, curr):
    totals = {}
    for key in curr:
        totals[key] = curr[key] - orig[key]
    return totals

interface = sys.argv[1]
ts_prev = time.monotonic()
orig = get_statistics(interface)
prev = orig

while True:
    time.sleep(1)
    ts_curr = time.monotonic()
    stats = get_statistics(interface)
    ts_delta =  ts_curr - ts_prev
    rates = get_rates(prev, stats, ts_delta)
    totals = get_totals(orig, stats)
    ts_prev = ts_curr
    prev = stats
    print("RX Bytes   %2.2f Bps (%d total)" % (rates['rx_bytes_phy'], totals['rx_bytes_phy']))
    print("TX Bytes   %2.2f Bps (%d total)" % (rates['tx_bytes_phy'], totals['tx_bytes_phy']))
    print("RX Packets %2.2f pps (%d total)" % (rates['rx_packets_phy'], totals['rx_packets_phy']))
    print("TX Packets %2.2f pps (%d total)" % (rates['tx_packets_phy'], totals['tx_packets_phy']))
